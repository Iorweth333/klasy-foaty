%%%-------------------------------------------------------------------
%%% @author Karol
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 17. lis 2018 22:57
%%%-------------------------------------------------------------------
-module(relacja_zaleznosci).
-author("Karol").

%% API
-export([wszystkie_relacje/1, relacje_zaleznosci/2, symetria_relacji/1, foata/2]).

% alfabet A byłby np listą, tak samo relacje
% trzeba by stworzyć listę krotek każdy-z-każdym i odpalić diffa na tej liscie irelaci niezależności
% można odejmować listy: operator --

wszystkie_relacje([]) -> [];
wszystkie_relacje ([H|T]) ->
  RelList1 = [{H,H}],
  RelList2 = RelList1 ++ [{H,G} || G <- T],
  RelList2 ++ wszystkie_relacje(T).         %optymalizacja? a co to?



relacje_zaleznosci(Alfabet, Relacje_niezaleznosci) when is_list(Alfabet) and is_list(Relacje_niezaleznosci) ->
  wszystkie_relacje(Alfabet) -- Relacje_niezaleznosci.

symetria_relacji([]) -> [];
symetria_relacji ( [{A,A} | T]) ->
  [{A,A}] ++ symetria_relacji(T);
symetria_relacji ( [{A,B} | T]) ->
  [{A,B}, {B,A}] ++ symetria_relacji(T).


-record (stos, {nazwa_stosu, lista_elementow}).

foata (Slowo, Relacje_zal) ->
  Owols = lists:reverse(Slowo),
  Stosy = uzupelnij_stosy(Owols, Relacje_zal,[]),
  Same_stosy = lists:flatmap (fun ({_,_,X}) -> [X] end, Stosy),
  Klasy_foaty = klasy_foaty(Same_stosy, []),
  Funkcja = fun (X) -> case length(X) > 0 of
                         true -> true;
                         false -> false
                       end
    end,
  lists:filter(Funkcja, Klasy_foaty).

%niedokonczone

klasy_foaty([], Klasy) -> Klasy;
klasy_foaty(Same_stosy, Klasy) ->             %Klasy to lista list, każda z wewnętrznych list jest osobną klasą
  Nowa_klasa = [X || [X|T] <- Same_stosy, X /= $*], %wykorzystanie pierwszych wierszy
  Nowe_klasy = lists:append(Klasy, [Nowa_klasa]),
  Nowe_stosy = [T || [X|T] <- Same_stosy],          %odcięcie pierwszych wierszy
  klasy_foaty(Nowe_stosy, Nowe_klasy).

uzupelnij_stosy([], _, Stosy) -> Stosy;
uzupelnij_stosy(Slowo,Relacje,[]) ->
  Stosy = inicjalizacja_stosow(Slowo, []),
  uzupelnij_stosy(Slowo,Relacje,Stosy);
uzupelnij_stosy([S|Lowo], [R|Elacje_z], Stosy) ->  %slowo juz odwrocone
  Nowe_stosy = wrzuc_na_stos(Stosy, S, S),          %wrzucanie elementu na jego stos
  Zalezne = [Y || {X,Y} <- [R|Elacje_z], X == S, Y /= S ],
  Uzupelnione_stosy = wrzuc_gwiazdki(Nowe_stosy, Zalezne),
  uzupelnij_stosy(Lowo, [R|Elacje_z], Uzupelnione_stosy).






inicjalizacja_stosow([], Stosy) -> Stosy;
inicjalizacja_stosow([A|Kcje], Stosy) ->
  IsMemberAlready = lists:keymember(A,2,Stosy),
  case IsMemberAlready of
    true ->
      inicjalizacja_stosow(Kcje, Stosy);
    false ->
      Nowe_stosy = lists:append([#stos{nazwa_stosu = A, lista_elementow = []}], Stosy),
      inicjalizacja_stosow(Kcje, Nowe_stosy)
  end.


wrzuc_gwiazdki(Stosy,[]) -> Stosy;
wrzuc_gwiazdki(Stosy, [Z|Alezne])->
  Stos = lists:keyfind(Z, 2, Stosy),
  Stos_z_gwiazdka = wrzuc_na_stos(Stos, $*),
  Uzupelnione_stosy = lists:keyreplace(Z, 2, Stosy, Stos_z_gwiazdka),
  wrzuc_gwiazdki(Uzupelnione_stosy, Alezne).

wrzuc_na_stos(Stosy, Nazwa_stosu, Element) when is_list(Stosy) ->
  Stos = lists:keyfind(Nazwa_stosu, 2, Stosy),
  Uzupelniony_stos = wrzuc_na_stos(Stos, Element),
  lists:keyreplace(Nazwa_stosu, 2, Stosy, Uzupelniony_stos).

wrzuc_na_stos(Stos, Element) when is_tuple(Stos)->
  #stos{nazwa_stosu = Stos#stos.nazwa_stosu, lista_elementow = lists:append(Stos#stos.lista_elementow, [Element])}.

%--------------------------------------------------------------------------------------------------------
%dolacz_relacje(Relacje, ) -> [ ||]
%dolacz_relacje(Relacje, Element1, Element2) -> Relacje ++ {Element1, Element2}.

%diff(L1, L2) ->
%  filter(fun(X) -> not member(X, L2) end, L1).

%   compile:file("src/relacja_zaleznosci.erl", debug_info).
%   debugger:start().
%   c("src/relacja_zaleznosci.erl").

%   Alfabet = [$a,$b,$c,$d].
%   Rel_niez = [{$a,$d}, {$b,$c}].
%   Rel_zal = relacja_zaleznosci:relacje_zaleznosci(Alfabet, Rel_niez).
%   Rel_zal_sym = relacja_zaleznosci:symetria_relacji(Rel_zal).
%   F = relacja_zaleznosci:foata("badacb", Rel_zal_sym).
